use crate::{cargo::Lockfile, sonar};
use eyre::{Context, Result};
use std::{fs::File, path::PathBuf};
use tracing::info;

const OUTDATED_ENGINE: &str = "outdated";

#[derive(Debug, serde::Deserialize)]
pub struct CrateMetadata {
    pub crate_name: String,
    pub dependencies: Vec<Metadata>,
}

#[derive(Debug, serde::Deserialize)]
pub struct Metadata {
    pub name: String,
    pub project: String,
    pub compat: String,
    pub latest: String,
    pub kind: Option<String>,
    pub platform: Option<String>,
}

#[derive(Debug)]
pub struct Outdated<'lock> {
    json: PathBuf,
    lockfile: &'lock Lockfile,
}

impl<'lock> Outdated<'lock> {
    pub fn new<P>(json: P, lockfile: &'lock Lockfile) -> Self
    where
        P: Into<PathBuf>,
    {
        Self {
            json: json.into(),
            lockfile,
        }
    }

    #[allow(clippy::needless_pass_by_value)]
    fn to_issue(&self, crate_name: &str, metadata: Metadata) -> sonar::Issue {
        let message = format!(
            "'{}' in crate '{}' is outdated and can be updated up to '{}'",
            metadata.name, crate_name, metadata.latest
        );
        let file_path = self.lockfile.lockfile_path.to_string_lossy().to_string();
        let text_range = self.lockfile.dependency_range(metadata.name.as_str());
        let primary_location = sonar::Location {
            message,
            file_path,
            text_range,
        };
        sonar::Issue {
            engine_id: OUTDATED_ENGINE.to_owned(),
            rule_id: format!("{}::{}", OUTDATED_ENGINE, metadata.name),
            severity: sonar::Severity::Minor,
            r#type: sonar::Type::CodeSmell,
            primary_location,
            secondary_locations: Vec::new(),
        }
    }

    pub(crate) fn to_issues(&self, crate_metadata: CrateMetadata) -> sonar::Issues {
        crate_metadata
            .dependencies
            .into_iter()
            .map(|dependency| self.to_issue(&crate_metadata.crate_name, dependency))
            .collect()
    }
}

impl std::convert::TryInto<sonar::Issues> for Outdated<'_> {
    type Error = eyre::Error;

    fn try_into(self) -> Result<sonar::Issues> {
        let file = File::open(&self.json).with_context(|| {
            format!(
                "failed to open 'cargo-outdated' report from '{:?}' file",
                self.json
            )
        })?;
        let report = serde_json::Deserializer::from_reader(file)
            .into_iter()
            .try_fold(
                sonar::Issues::default(),
                |mut acc, crate_metadata| -> Result<_, eyre::Error> {
                    let crate_metadata: CrateMetadata =
                        crate_metadata.context("failed to be parsed from JSON")?;
                    let issues = self.to_issues(crate_metadata);
                    acc.extend(issues);
                    Ok(acc)
                },
            )?;
        info!("{} sonar issues created", report.len());
        Ok(report)
    }
}
