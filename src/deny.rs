use crate::{cargo::Lockfile, sonar};
use eyre::{Context as _, Result};
use skip_error::SkipError;
use std::{
    fs::File,
    io::{BufRead as _, BufReader},
    path::PathBuf,
};
use tracing::info;

const DENY_ENGINE: &str = "deny";

#[derive(Debug, serde::Deserialize)]
// ALLOW: Unused fields are part of the deserialized schema from 'cargo-deny'
#[allow(dead_code)]
pub struct GraphNode {
    name: String,
    version: String,
    #[serde(default)]
    kind: String,
    #[serde(default)]
    repeat: bool,
    #[serde(default)]
    parents: Vec<GraphNode>,
}

#[derive(Debug, serde::Deserialize)]
// ALLOW: Unused fields are part of the deserialized schema from 'cargo-deny'
#[allow(dead_code)]
pub struct Label {
    line: usize,
    column: usize,
    message: String,
    span: String,
}

type AdvisoryReport = rustsec::advisory::Metadata;
#[derive(Debug, serde::Deserialize)]
struct LicenseReport {
    code: String,
    graphs: Vec<GraphNode>,
    message: String,
    labels: Vec<Label>,
}

#[derive(Debug, serde::Deserialize)]
#[serde(untagged)]
enum DenyReport {
    Advisory(Box<AdvisoryReport>),
    License(Box<LicenseReport>),
}

#[derive(Debug)]
pub struct Deny<'lock> {
    json: PathBuf,
    lockfile: &'lock Lockfile,
}

impl<'lock> Deny<'lock> {
    pub fn new<P>(json: P, lockfile: &'lock Lockfile) -> Self
    where
        P: Into<PathBuf>,
    {
        Self {
            json: json.into(),
            lockfile,
        }
    }
    fn to_issue(&self, deny_report: DenyReport) -> Option<sonar::Issue> {
        match deny_report {
            DenyReport::Advisory(advisory) => {
                let rule_id = format!("{}::{}", DENY_ENGINE, advisory.id);
                let message = format!(
                    "{} (see https://github.com/rustsec/advisory-db/blob/main/crates/{}/{}.md)",
                    advisory.title, advisory.package, advisory.id
                );
                let file_path = self.lockfile.lockfile_path.to_string_lossy().to_string();
                let text_range = self.lockfile.dependency_range(advisory.package.as_str());
                let primary_location = sonar::Location {
                    message,
                    file_path,
                    text_range,
                };
                let secondary_locations = Vec::new();
                let issue = sonar::Issue {
                    engine_id: DENY_ENGINE.to_owned(),
                    rule_id,
                    severity: sonar::Severity::Major,
                    r#type: sonar::Type::Vulnerability,
                    primary_location,
                    secondary_locations,
                };
                Some(issue)
            }
            DenyReport::License(license) => {
                let graph_node = license.graphs.get(0)?;
                let file_path = self.lockfile.lockfile_path.to_string_lossy().to_string();
                let text_range = self.lockfile.dependency_range(graph_node.name.as_str());
                let message = license
                    .labels
                    .iter()
                    .fold(license.message.clone(), |message, label| {
                        format!("{}.\n`{}` {}", message, label.span, label.message)
                    });
                let primary_location = sonar::Location {
                    message,
                    file_path,
                    text_range,
                };
                let secondary_locations = Vec::new();
                let issue = sonar::Issue {
                    engine_id: DENY_ENGINE.to_owned(),
                    rule_id: format!("{}::{}", DENY_ENGINE, license.code),
                    severity: sonar::Severity::Info,
                    r#type: sonar::Type::CodeSmell,
                    primary_location,
                    secondary_locations,
                };
                Some(issue)
            }
        }
    }
}

impl std::convert::TryInto<sonar::Issues> for Deny<'_> {
    type Error = eyre::Error;

    fn try_into(self) -> Result<sonar::Issues> {
        let file = File::open(&self.json).with_context(|| {
            format!(
                "failed to open 'cargo-deny' report from '{:?}' file",
                self.json
            )
        })?;
        let reader = BufReader::new(file);
        let issues: sonar::Issues = reader
            .lines()
            .flatten()
            .flat_map(|line| serde_json::from_str::<serde_json::Value>(&line))
            .filter_map(|value| value.get("fields").map(ToString::to_string))
            .map(|s| {
                serde_json::from_str::<DenyReport>(&s)
                    .with_context(|| format!("failed to deserialize '{}'", s))
            })
            .skip_error_and_debug()
            .filter_map(|diagnostic| self.to_issue(diagnostic))
            .collect();
        info!("{} sonar issues created", issues.len());
        Ok(issues)
    }
}
